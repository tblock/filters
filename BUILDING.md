# Create a new filter list

This file describes the process for adding a new filter list to this repository.

## 1. Create the filter list

The first step is to create a new directory for our filter list. This can be easily achieved by executing:

``` bash
$ ./build.sh --create "example"
> Creating filter list: example
 + Copying templates to: src/example
```

We can now see the components of your new filter list under:

``` bash
$ tree src/example
src/example
├── build_script.py
├── custom.txt
├── header.txt
├── README.md
└── sources.txt
```

## 2. Edit the header

The header is a few comment lines that are added at the top of our filter list. It usually contains useful information, such as the title of the filter list, the version of the file or licensing information.

To edit the header, let's open `src/example/header.txt`. The script executed in step 1 already wrote the following in it:

``` bash
# Title: My filter list
# Version: $DATE
# Description: A short description
# Homepage: https://example.org
# License: Link to license or SDPX-identifer

```

We can now update all these values, and add more if you want.

> **Note:** if you want the script to automatically update the version of your filter list, you should leave the `$DATE` field.

## 3. Add sources

Now, we need to add some sources to add to our filter list. Let's open `src/example/sources.txt` to do so. We can see that there is currently no source in it:

``` bash
# Write the sources you want to include in your filter list here
# One URL per line
# Note that all URLs MUST point to a plain text file

```

We can add as many sources as we want. The only thing that we need to respect is that each URL must be written in a new line. If a line is not a valid URL (or a comment), it will cause the script to crash.

## 4. Add custom entries

Now, we have maybe or own entries that we want to add to our filter list. Let's open `src/example/custom.txt`:

``` toml
[allow]
# Write your allowing rules here

[block]
# Write your blocking rules here

```

Let's add a few entires in there (we can add as many as we want):

``` toml
[allow]
# Write your allowing rules here
example.org
example.com

[block]
# Write your blocking rules here
creepy.site
another-creepy.site
```

> **Note:** only valid domains will be processed. IP addresses, comments, and other strings won't be added to our filter list.

## 5. Add dynamic entries using a Python script

Sometimes, we need a custom script to generate additional entries while building our filter list. A good example is to retrieve a list of domains stored in a database.

Let's have a look at `src/example/build_script.py`:

``` python
#!/usr/bin/python3

import os

# Do not change this line
OUT_FILE = os.path.join(os.path.dirname(__file__), "custom_from_script.txt")

with open(OUT_FILE, "wt") as f:
    # Write your rules in this file
    f.write("# Some rule\n")
```

We can write our own script in this file. The only things that we need to respect are:
- The output file MUST be `OUT_FILE` and its value MUST NOT change.
- The output file MUST be in the same format as `src/example/custom.txt`, which means that the domains to allow are in the `[allow]` section, while the domains to block are in the `[block]` section.

## 6. Edit the README

Previously, we noticed a file called `src/example/README.md`. This is a useful place to write a description of your filter list, to list licenses and attributions, and to provide more information about what your filter list does. Of course, you can delete this file if you don't need it.

## 7. Building the filter list

Now that everything is configured, let's build our filter list by running:

``` bash
$ ./build.sh -b "example"
> Building filter list: example
 + Adding header from: src/example/header.txt
 + Adding entries from: src/example/custom.txt
 + Executing build script: src/example/build_script.py
 + Building and converting filter list in: filters/example
```

We can now find our filter list (in several formats) under:

``` bash
$ tree filters/example
filters/example
├── abp.txt
├── domains.txt
├── hosts
└── tblock2.txt
```
