# TBlock Light Blocklist

Lightweight protection designed for Windows

## Subscribe

- [ABP/uBO](https://codeberg.org/tblock/filters/raw/branch/main/filters/light/abp.txt)
- [Hosts/pi-hole](https://codeberg.org/tblock/filters/raw/branch/main/filters/light/hosts)
- [List](https://codeberg.org/tblock/filters/raw/branch/main/filters/light/domains.txt)
- [TBlock](https://codeberg.org/tblock/filters/raw/branch/main/filters/light/tblock2.txt)
