# TBlock Security List

Basic protection against spam, scam and malicious domains

## Subscribe

- [ABP/uBO](https://codeberg.org/tblock/filters/raw/branch/main/filters/security/abp.txt)
- [Hosts/pi-hole](https://codeberg.org/tblock/filters/raw/branch/main/filters/security/hosts)
- [List](https://codeberg.org/tblock/filters/raw/branch/main/filters/security/domains.txt)
- [TBlock](https://codeberg.org/tblock/filters/raw/branch/main/filters/security/tblock2.txt)

## Credits

| Source | License(s) |
| --- | --- |
| [add.Risk](https://github.com/FadeMind/hosts.extras) | [GPLv3](https://www.gnu.org/licenses/gpl-3.0.txt) |
| [add.Spam](https://github.com/FadeMind/hosts.extras) | [GPLv3](https://www.gnu.org/licenses/gpl-3.0.txt) |
| [Badd Boyz Hosts](https://github.com/mitchellkrogza/Badd-Boyz-Hosts) | [MIT License](https://raw.githubusercontent.com/mitchellkrogza/Badd-Boyz-Hosts/master/LICENSE.md) |
| [blackbook](https://github.com/stamparm/blackbook) | [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/) |
