# Title: TBlock Security List
# Version: $DATE
# Description: Basic protection against spam, scam and malicious domains
# Homepage: https://tblock.me
# License: https://codeberg.org/tblock/filters/src/branch/main/LICENSE
