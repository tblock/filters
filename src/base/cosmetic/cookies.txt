! Google Analytics
$cookie=/__utm[a-z]/

! Optimizely
$cookie=optimizelyEndUserId

! Facebook
||facebook.com^$third-party,cookie=c_user

! Microsoft
$cookie=MicrosoftApplicationsTelemetryFirstLaunchTime
$cookie=MicrosoftApplicationsTelemetryDeviceId

! TrafficJunky
$cookie=trafficJunkyPopsBackUrl

! Other cookies
$cookie=mParticleId
$cookie=uber_sites_geolocalization
$cookie=PersonalizationCookie
