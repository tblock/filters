# TBlock Base List

Basic protection against ads and trackers

## Subscribe

- [ABP/uBO](https://codeberg.org/tblock/filters/raw/branch/main/filters/base/abp.txt)
- [Hosts/pi-hole](https://codeberg.org/tblock/filters/raw/branch/main/filters/base/hosts)
- [List](https://codeberg.org/tblock/filters/raw/branch/main/filters/base/domains.txt)
- [TBlock](https://codeberg.org/tblock/filters/raw/branch/main/filters/base/tblock2.txt)

## Credits

| Source | License(s) |
| --- | --- |
| [EasyList](https://easylist.to) | [GPLv3+](https://www.gnu.org/licenses/gpl-3.0.html)/[CC BY-SA 3.0+](https://creativecommons.org/licenses/by-sa/3.0/) |
| [EasyPrivacy](https://easylist.to) | [GPLv3+](https://www.gnu.org/licenses/gpl-3.0.html)/[CC BY-SA 3.0+](https://creativecommons.org/licenses/by-sa/3.0/) |
