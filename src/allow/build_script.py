#!/usr/bin/python3

import os
import urllib3
import tblock
from tblock.filters import Filter

# Do not change this line
OUT_FILE = os.path.join(os.path.dirname(__file__), "custom_from_script.txt")

allow_list = []

for f in tblock.get_all_filter_lists(from_repo_only=True):
    if not urllib3.util.parse_url(Filter(f).source).host in allow_list:
        allow_list.append(urllib3.util.parse_url(Filter(f).source).host)
    allow_list.sort()

with open(OUT_FILE, "wt") as f:
    f.write("[allow]")
    for i in allow_list:
        f.write("\n" + i)
