# TBlock Allow List

List that unblocks domains required by TBlock in order to work properly

## Subscribe

- [ABP/uBO](https://codeberg.org/tblock/filters/raw/branch/main/filters/allow/abp.txt)
- [TBlock](https://codeberg.org/tblock/filters/raw/branch/main/filters/allow/tblock2.txt)
