# TBlock Anti-AI List

Short list that blocks common AI and deepfake servers

## Subscribe

- [ABP/uBO](https://codeberg.org/tblock/filters/raw/branch/main/filters/anti-ai/abp.txt)
- [Hosts/pi-hole](https://codeberg.org/tblock/filters/raw/branch/main/filters/anti-ai/hosts)
- [List](https://codeberg.org/tblock/filters/raw/branch/main/filters/anti-ai/domains.txt)
- [TBlock](https://codeberg.org/tblock/filters/raw/branch/main/filters/anti-ai/tblock2.txt)
