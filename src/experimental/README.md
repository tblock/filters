# TBlock Experimental List

List used for experimental purposes (can possibly break some web pages)

## Subscribe

- [ABP/uBO](https://codeberg.org/tblock/filters/raw/branch/main/filters/experimental/abp.txt)
- [Hosts/pi-hole](https://codeberg.org/tblock/filters/raw/branch/main/filters/experimental/hosts)
- [List](https://codeberg.org/tblock/filters/raw/branch/main/filters/experimental/domains.txt)
- [TBlock](https://codeberg.org/tblock/filters/raw/branch/main/filters/experimental/tblock2.txt)
