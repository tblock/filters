# Candidates for tblock-base

[block]

# nature trackers (flagged by ddgtrackerradar so might be false-positive)
geo-gcp.cdn.springernature.io
verify.nature.com
cmp.nature.com

# Potential tracking servers
# This domain is in the divested list
brightcove.net
# And this subdomain and CNAME record were found
players.brightcove.net
players.brightcove.net.edgekey.net

# Potential tracking servers
# will be merged by the end of the year if no issue is found
realtime.ably.io
d1bxh8uas1mnw7.cloudfront.net
