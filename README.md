<p align="center">
<img align="center" src="https://codeberg.org/tblock/filters/raw/branch/src/icon.png" alt="Logo" width="220px">
</p>
<p align="center">
<h1 align="center">TBlock Filter Lists</h1>
</p>
<p align="center">
Official filter lists for use with <a href="https://tblock.me">TBlock</a> or other ad blockers.
</p>


## Lists

- [TBlock Allow List](src/allow/README.md): List that unblocks domains required by TBlock in order to work properly
- [TBlock Anti-AI List](src/anti-ai/README.md):  A small list that blocks common AI servers
- [TBlock Base List](src/base/README.md): Basic protection against ads and trackers
- [TBlock Security List](src/security/README.md): Basic protection against spam, scam and malicious domains
- [TBlock Experimental List](src/experimental/README.md): List used for experimental purposes (can possibly break some web pages)

## How are these lists built?

Check out [BUILDING.md](BUILDING.md) for more information.

## License


[![GPLv3](https://www.gnu.org/graphics/gplv3-with-text-136x68.png)](https://www.gnu.org/licenses/gpl-3.0)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
