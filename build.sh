#!/bin/sh

set -e

bold=$(tput bold)
normal=$(tput sgr0)
srcdir="src"
builddir="filters"
cachedir=".cache"
templatedir="default"

build_()
{
    if [ -d "${srcdir}/${1}" ]; then
        echo "${bold}> Building filter list: ${1}${normal}"

        # Recreate cache directory
        if [ -d ${cachedir} ]; then
            rm -rf ${cachedir}
        fi
        mkdir -p ${cachedir}

        # Create new filter list with correct date and description
        if [ -f "${srcdir}/${1}/header.txt" ]; then
            echo " + Adding header from: ${srcdir}/${1}/header.txt"
            header=$(cat "${srcdir}/${1}/header.txt")
            echo "${header/\$DATE/$(date +%y%m%d)}" > "${cachedir}/${1}.txt"
        fi

        # Add custom entries to the new filter list
        if [ -d "${srcdir}/${1}/custom/" ]; then
            for x in "${srcdir}/${1}/custom/"*.txt; do
                echo " + Adding entries from: ${x}"
                tblockc -C "${x}" -qzs tblock -o ${cachedir}/out.txt -i tblock
                cat ${cachedir}/out.txt >> "${cachedir}/${1}.txt"
            done
        fi

        # Execute build script if needed
        if [ -f "${srcdir}/${1}/build_script.py" ]; then
            echo " + Executing build script: ${srcdir}/${1}/build_script.py"
            python3 "${srcdir}/${1}/build_script.py"
            tblockc -C "${srcdir}/${1}/custom_from_script.txt" -qzs tblock -o ${cachedir}/out.txt -i tblock
            cat ${cachedir}/out.txt >> "${cachedir}/${1}.txt"
        elif [ -f "${srcdir}/${1}/build_script.sh" ]; then
            echo " + Executing build script: ${srcdir}/${1}/build_script.sh"
            /bin/sh "${srcdir}/${1}/build_script.sh"
            tblockc -C "${srcdir}/${1}/custom_from_script.txt" -qzs tblock -o ${cachedir}/out.txt -i list
            cat ${cachedir}/out.txt >> "${cachedir}/${1}.txt"
        fi

        # Compile cosmetic rules if they exist
        if [ -d "${srcdir}/${1}/cosmetic" ]; then
            for x in "${srcdir}/${1}/cosmetic/"*.txt; do
                echo " + Adding cosmetic rules from: ${x}"
                cat "${x}" | sed '/^$/d' | sed '/^!/d' >> ${cachedir}/${1}_cosmetic.txt
            done
        fi

        # Download the sources, convert them and append them into the correct file
        if [ -f "${srcdir}/${1}/sources.txt" ]; then
            (cat "${srcdir}/${1}/sources.txt"; echo) | while read x; do
            if [ "${x:0:1}" != "#" ] && [ "${x}" != "" ]; then
                echo " + Adding entries from: ${x}"
                rm -f ${cachedir}/tmp.txt ${cachedir}/out.txt
                wget ${x} -qO ${cachedir}/tmp.txt
                tblockc -C ${cachedir}/tmp.txt -qzs tblock -o ${cachedir}/out.txt
                echo "# --- Rules from: ${x} ---" >> "${cachedir}/${1}.txt"
                cat ${cachedir}/out.txt >> "${cachedir}/${1}.txt"
            fi
        done
        fi

        # Convert the filter list into new ones
        if [ -f "${cachedir}/${1}.txt" ]; then
            echo " + Building and converting filter list in: ${builddir}/${1}"
            mkdir -p "${builddir}/${1}"
            tblockc -C "${cachedir}/${1}.txt" -qzsc adblockplus -o "${builddir}/${1}/abp.txt" -i tblock
            if [ -f "${cachedir}/${1}_cosmetic.txt" ]; then
                cat "${cachedir}/${1}_cosmetic.txt" >> "${builddir}/${1}/abp.txt"
            fi
            tblockc -C "${cachedir}/${1}.txt" -qzsc list -o "${builddir}/${1}/domains.txt" -i tblock
            tblockc -C "${cachedir}/${1}.txt" -qzsc hosts -o "${builddir}/${1}/hosts" -i tblock
            tblockc -C "${cachedir}/${1}.txt" -qzsc tblock -o "${builddir}/${1}/tblock2.txt" -i tblock
        fi
    else
        echo "Error: ${1} does not exist."
        echo "Run: ${0} -c ${1} to create it."
        false
    fi
}

build_all_()
{
    for x in ${srcdir}/*/; do
        x="${x/${srcdir}\//}"
        build_ "${x/\//}"
    done
}

create_()
{
    if [ -d "${srcdir}/${1}" ]; then
        echo "Error: ${1} already exists."
        echo "Run: ${0} -d ${1} to delete it."
        false
    else
        echo "${bold}> Creating filter list: ${1}${normal}"
        mkdir -p "${srcdir}/${1}"
        echo " + Copying templates to: ${srcdir}/${1}"
        for x in ${templatedir}/*; do
            cp -r "${x}" "${srcdir}/${1}"
            echo "custom_from_script.txt" > "${srcdir}/${1}/.gitignore"
        done
    fi
}

delete_()
{
    if [ -d "${srcdir}/${1}" ]; then
        echo "${bold}> Removing filter list: ${1}${normal}"
        read -p " : Are you sure? [y/n] " -r confirm_
        if [ "${confirm_}" == "y" ] || [ "${confirm_}" == "Y" ]; then
            read -p " : Also remove built filter list? [y/n] " -r confirm_2_
            echo " + Removing source: ${srcdir}/${1}"
            rm -rf "${srcdir}/${1}"
            if [ "${confirm_2_}" == "y" ] || [ "${confirm_2_}" == "Y" ]; then
                echo " + Removing builds: ${builddir}/${1}"
                rm -rf "${builddir}/${1}"
            fi
        else
            false
        fi

    else
        echo "Error: ${1} does not exist."
        echo "Run: ${0} -c ${1} to create it."
        false
    fi
}

help_()
{
    echo "${0} [OPTION] [LIST]"
    echo "options:"
    echo " -a, --build-all     Build all filter lists"
    echo " -b, --build  LIST   Build a given filter list"
    echo " -c, --create LIST   Create a new filter list"
    echo " -d, --delete LIST   Delete a given filter list"
    echo " -h, --help          Show this help page"
    echo " -l, --list          List all filter lists"
}

list_()
{
    for x in ${srcdir}/*/; do
        x="${x/${srcdir}\//}"
        echo "${x/\//}"
    done
}

case "${1}" in
    "-a")
        build_all_
        ;;
    "--build-all")
        build_all_
        ;;
    "-b")
        build_ $2
        ;;
    "--build")
        build_ $2
        ;;
    "-c")
        create_ $2
        ;;
    "--create")
        create_ $2
        ;;
    "-d")
        delete_ $2
        ;;
    "--delete")
        delete_ $2
        ;;
    "-l")
        list_
        ;;
    "--list")
        list_
        ;;
    "-h")
        help_
        ;;
    "--help")
        help_
        ;;
    *)
        echo "Run: ${0} -h to show help page."
        false
        ;;
esac
