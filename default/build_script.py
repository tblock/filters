#!/usr/bin/python3

import os

# Do not change this line
OUT_FILE = os.path.join(os.path.dirname(__file__), "custom_from_script.txt")

with open(OUT_FILE, "wt") as f:
    # Write your rules in this file
    f.write("# Some rule\n")
